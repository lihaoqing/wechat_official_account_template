import Vue from 'vue'
import {
  Switch,
  Area,
  Popup,
  Cell,
  Toast,
  Overlay,
  Field,
  Swipe,
  SwipeItem,
  Lazyload,
  DatetimePicker,
  Button,
  Picker,
  List
} from "vant";

Vue.use(Switch);
Vue.use(Area);
Vue.use(Popup);
Vue.use(Cell);
Vue.use(Toast);
Vue.use(Overlay);
Vue.use(Field);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Lazyload);
Vue.use(DatetimePicker);
Vue.use(Button);
Vue.use(Picker);
Vue.use(List);
