import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// Vue-router在3.1之后把$router.push()方法改为了Promise。所以假如没有回调函数，错误信息就会交给全局的路由错误处理, 报NavigationDuplicated的错误
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}

const routes = [{
  path: '/',
  redirect: '/index',
  component: () => import( /* webpackChunkName: "myapp" */ '../App.vue'),
  children: [{
    path: '/index',
    component: () => import( /* webpackChunkName: "index" */ '../views/Index.vue'),
    meta: {
      title: '首页',
    }
  }]
}]

const router = new VueRouter({
  routes
})

export default router
