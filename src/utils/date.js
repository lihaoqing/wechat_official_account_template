function getLocalTime(ns) {
  //needTime是整数，否则要parseInt转换
  var time = new Date(parseInt(ns)); //根据情况*1000
  var y = time.getFullYear();
  var m = time.getMonth() + 1;
  var d = time.getDate();
  var h = time.getHours();
  var mm = time.getMinutes();
  var s = time.getSeconds();
  return (
          y +
          "-" +
          add0(m) +
          "-" +
          add0(d) +
          " " +
          add0(h) +
          ":" +
          add0(mm) +
          ":" +
          add0(s)
  );
}
var formatDateTime = function (date) {
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? ('0' + m) : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  var h = date.getHours();
  h = h < 10 ? ('0' + h) : h;
  var minute = date.getMinutes();
  minute = minute < 10 ? ('0' + minute) : minute;
  var second = date.getSeconds();
  second = second < 10 ? "0" + second : second;
  return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' +second;
}
//小于10的补零操作
function add0(m) {
  return m < 10 ? "0" + m : m;
}
export default { getLocalTime, formatDateTime }