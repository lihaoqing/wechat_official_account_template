import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import qs from 'qs'

import './plugins/vant.js'
import './request/index.js'
import 'lib-flexible/flexible.js'
import regionList from './utils/region.js'

// 阻止启动生产消息
Vue.config.productionTip = false
// var env = process.env.NODE_ENV
Vue.prototype.regionList = regionList
Vue.prototype.Qs = qs


// 路由拦截
router.beforeEach((to,from,next) => {
  // 更换页面title
  if(to.meta.title) {
    document.title = to.meta.title
  }else {
    document.title = "我是页面title"
  }
  next()
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
